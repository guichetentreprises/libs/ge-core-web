package fr.ge.core.rest;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.fest.assertions.Assertions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;

public class RestClientExceptionMapperTest {

  /**
   * End point adress.
   */
  private static final String ENDPOINT_ADDRESS = "local://calculator/";

  /**
   * The server.
   */
  private static Server server;

  /**
   * Initialize the test class.
   */
  @BeforeClass
  public static void initialize() {
    startServer();
  }

  /**
   * Start the server.
   */
  private static void startServer() {
    JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
    sf.setResourceClasses(Calculator.class);
    List < Object > providers = new LinkedList < Object >();
    providers.add(new RestClientExceptionMapper());
    providers.add(new JacksonJaxbJsonProvider());
    sf.setResourceProvider(Calculator.class, new SingletonResourceProvider(new CalculatorImpl()));
    sf.setProviders(providers);
    sf.setAddress(ENDPOINT_ADDRESS);
    server = sf.create();
  }

  /**
   * Stop and Destroy the server at the end.
   */
  @AfterClass
  public static void destroy() {
    server.stop();
    server.destroy();
  }

  @Test
  public void testFromResponse500() {
    List < Object > providers = new LinkedList < Object >();
    providers.add(new RestClientExceptionMapper());
    Calculator calculator = JAXRSClientFactory.create(ENDPOINT_ADDRESS, Calculator.class, providers);
    try {
      calculator.divide(4, 0);
    } catch (TechniqueException ex) {
      assertEquals("Status: 500, method: divide, Endpoint adress: " + ENDPOINT_ADDRESS, ex.getMessage());
    } catch (FunctionalException e) {
      // -->No assert
      Assertions.assertThat(e);
    }
  }

}

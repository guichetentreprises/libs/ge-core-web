package fr.ge.core.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;

@Path("/calculator")
public interface Calculator {

  @GET
  @Path("/divide/{a}/{b}")
  double divide(@PathParam("a") double a, @PathParam("b") double b) throws FunctionalException, TechniqueException;
}

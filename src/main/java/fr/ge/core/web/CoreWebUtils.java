package fr.ge.core.web;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;

import fr.ge.core.log.GestionnaireTrace;

/**
 * Utility class for web applications.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class CoreWebUtils {

  /** The technical logger. */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /**
   * Private constructor.
   */
  private CoreWebUtils() {
  }

  /**
   * Encode the input URL using UTF-8 encoding.
   * 
   * @param request
   *          The HttpServletRequest
   * @return The URL encoded using UTF-8 or null
   * @throws UnsupportedEncodingException
   */
  public static String encodeURL(final HttpServletRequest request) {
    try {
      final String currentUrl = request.getRequestURL().toString();
      return URLEncoder.encode(currentUrl, StandardCharsets.UTF_8.toString());
    } catch (Exception e) {
      LOGGER_TECH.error("An error occured when trying to encode the request url");
    }
    return null;
  }
}

package fr.ge.core.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;

import fr.ge.core.log.GestionnaireTrace;

/**
 * Servlet to handle Error 500 in web applications.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
// XXX LAB : virer toutes ces concaténations de string avec des +
@WebServlet("/ErrorRuntimeHandler")
public class RuntimeErrorHandler extends HttpServlet {

    /** UID. */
    private static final long serialVersionUID = 7189180866814755771L;

    /** La constante LOGGER_FONC. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /**
     * {@inheritDoc}
     */
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        // XXX LAB : les toString sur les listes produisent des adresse mémoires
        // et pas du contenu
        // utilisable
        LOGGER_FONC.error("Servlet Runtime Error sur l'URL: {} avec les paramètres : {}. Headers : {}", request.getRequestURI(), request.getAttributeNames().toString(),
                request.getHeaderNames().toString());

        String contentMessageError;
        String contentLanguage;
        String title;
        String summary;
        if (Locale.FRENCH.equals(request.getLocale().getLanguage())) {
            contentMessageError = "<div class=\"page-content\">" + "<p>Nous mettons tout en œuvre pour résoudre au plus vite le problème technique.</p>" + "</div>";
            contentLanguage = "fr";
            title = "Erreur 500 | Erreur interne du serveur";
            summary = "Le site est temporairement indisponible";
        } else {
            contentMessageError = "<div class=\"page-content\">" + "<p>We are doing everything we can to resolve this technical problem as quickly as possible.</p></div>";
            contentLanguage = "en";
            title = "Error 500 | Internal server error";
            summary = "This site is temporarily unavailable";
        }

        // if the response has not been finalized
        if (response.isCommitted() == false) {
            // reset, so we can call the outputstream again
            response.reset();
            // Set response content type
            response.setContentType("text/html");

            PrintWriter responseWriter = response.getWriter();
            String docType = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";

            String htmlParameters = "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"" + contentLanguage + " xml:lang=" + contentLanguage + " class=\"no-js\">";

            String head = "<head><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width\"><title>" + title + "</title><meta name='robots' content='noindex,follow' />"
                    + "<link rel='stylesheet'  href='/errordocs/skin/style/style.css' type='text/css' media='all' /></head>";

            String body = "   <body class=\"home page page-id-17 page-template-default\">" + "<div id=\"page\" class=\"hfeed site\"><div id=\"sidebar-0\"></div>"
                    + "<div style=\"clear:both; content:'';\"></div><header id=\"masthead\" class=\"site-header\" role=\"banner\">"
                    + "<div id=\"logo\"><a href=\"/\"></a></div></header><div id=\"blocks_home\"><main id=\"main\" class=\"site-main\" role=\"main\">" + "<section class=\"error-404 not-found\">"
                    + "<div class=\"logo\"><a href=\"/\"  ></a></div><header class=\"page-header\"><h1 class=\"page-title\">" + summary + "</h1></header>" + contentMessageError
                    + "<footer id=\"colophon\" class=\"site-footer\" role=\"contentinfo\">" + "<div id=\"sidebar-2\">" + "<div id=\"widget-area\" class=\"widget-area\" role=\"complementary\">"
                    + "<aside id=\"custom_post_widget-10\" class=\"widget widget_custom_post_widget\">" + "<div class=\"site-info wdfoot\">"
                    + "<img src=\"/errordocs/skin/img/footer_logos.jpg\" alt=\"logos\" width=\"100%\" />" + "<img src=\"/errordocs/skin/img/footer_logos.jpg\" alt=\"logos\" width=\"100%\" />"
                    + "</div>" + "</aside>" + "</div>" + "</div>" + "<div style=\"clear:both; content:'';\"></div>" + "<div style=\"clear:both; content:'';\"></div>" + "</footer>" + "</section>"
                    + "</main>" + "<!-- .site-main -->" + "</div>" + "</div>" + "</div><!-- .content-area -->" + "</body>";
            responseWriter.println(docType + htmlParameters + head + body);
        }

    }

    /**
     * {@inheritDoc}
     */
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
